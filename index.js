import Koa from 'koa'
import cors from 'kcors'
import bodyParser from 'koa-bodyparser'
import error from 'koa-json-error'
import morgan from 'koa-morgan'
import Raven from 'raven'

import routes from './src/routes'

const { PORT, SENTRY_DSN, NODE_ENV } = process.env

Raven.config(SENTRY_DSN, {
  release: `dev-${Date.now()}`,
  environment: NODE_ENV,
  autoBreadcrumbs: {
    http: true,
    console: true
  }
}).install()

const app = new Koa()
app.use((ctx, next) => {
  Raven.mergeContext({
    req: ctx.request
  })

  return next()
})
app.use(error({
  postFormat: (err, formatted) => {
    const response = NODE_ENV === 'production' ? Raven.captureException(err) : undefined

    return {
      ...formatted,
      eventId: response
    }
  }
}))
app.use(morgan('dev'))

const options = {
  origin: '*',
  headers: ['Content-Type', 'Authorization', 'Accept-Language'],
  methods: ['POST', 'PUT', 'GET', 'DELETE', 'OPTIONS']
}

app.proxy = true
app.use(cors(options))
app.use(bodyParser())
app.use(routes())

app.listen(PORT || 4000, () => {
  console.log(`🌍 🖥 Running on port ${PORT || 4000}`);
})
