# oneorder-api

## Tech stack

* [Node.js](https://nodejs.org)
* Koa 2.0
* Babel/ES6
* ArangoDB

## Setup

```
brew install arangodb
npm install
npm run dev
```

Certain `.env` values may be required. See `./share/sample.env` for a partial list.