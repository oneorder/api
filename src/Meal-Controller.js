import moment from 'moment'
import { Model } from './db'
import Meal from './Meal-Model'

/*
  This file has a big hack in it where we import the raw database connection from the ORM. We do this becasue there's a bug in the ORM at present that prevents us from using arrays of sub-documents. So instead we add them manually with the driver.
*/

export async function postMeal (ctx, next) {
  const { body, body: { nutrients, images } } = ctx.request
  const db = Model._database
  const collection = db.collection('Meal')

  const meal = await Meal.add({
    ...body,
    date: moment(body.date).format('YYYY-MM-DD'),
    expiration: moment(body.expiration).format('YYYY-MM-DD'),
    nutrients: [],
    images: []
  })

  await meal.save()
  await meal.update()
  const document = await collection.document(meal._id)

  const addSubDocument = async (key, value) => {
    document[key].push(value)
    await collection.update(meal._id, document)
  }

  await nutrients && nutrients.map(async (nutrient, index) => {
    await addSubDocument('nutrients', nutrient)

    if (index + 1 === nutrients.length) {
      return Promise.resolve()
    }
  })

  await images && images.map(async (image, index) => {
    await addSubDocument('images', image)

    if (index + 1 === images.length) {
      return Promise.resolve()
    }
  })

  await collection.update(meal._id, document)

  const newMeal = await Meal.findOne({ where: { _id: meal._id } })

  ctx.body = { data: newMeal }
  ctx.status = 200

  await next()
}

export async function getMeals (ctx, next) {
  const { page, count, start, end } = ctx.request.query

  const startDate = start ? moment(start).format('YYYY-MM-DD') : moment().startOf('week').format('YYYY-MM-DD')
  const endDate = end ? moment(end).format('YYYY-MM-DD') : moment().endOf('week').format('YYYY-MM-DD')

  const db = await Model._database

  const limit = count || 50
  const skip = limit * (page ? page - 1 : 0)

  const cursor = await db.query({
    query: `
      for m in Meal
        filter m._removed != true
        filter m.date > @startDate && m.date <= @endDate
        sort m.date ASC
        limit ${skip}, ${limit}
        return m
    `,
    bindVars: { startDate, endDate }
  })

  const meals = await cursor.all()

  ctx.body = { data: meals }
  ctx.status = 200

  await next()
}

export async function deleteMeal (ctx, next) {
  const { meal } = ctx.params

  const mealToRemove = await Meal.findOne({ where: { _key: meal } })

  if (!mealToRemove) {
    ctx.throw('MEAL NOT FOUND')
  }

  await mealToRemove.remove()

  ctx.body = {
    data: mealToRemove
  }
  ctx.status = 200
  await next()
}

export async function getMeal (ctx, next) {
  const { meal } = ctx.params

  const requestedMeal = await Meal.findOne({ where: { _key: meal } })

  if (!requestedMeal) {
    ctx.throw('MEAL NOT FOUND')
  }

  ctx.body = {
    data: requestedMeal
  }
  ctx.status = 200
  await next()
}

export async function patchMeal (ctx, next) {
  const { request: { body }, params: { meal } } = ctx

  const db = Model._database
  const collection = db.collection('Meal')

  await collection.update(meal, body)

  const document = await collection.document(meal)

  ctx.body = {
    data: document
  }
  ctx.status = 200

  await next()
}
