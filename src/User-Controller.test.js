import ApiClient from './ApiClient'

import registerTestUser from './common/register-test-user'

let client
let deletedUserKey

test('registration', async () => {
  const register = await registerTestUser()
  client = await register.client

  expect(register.user).toHaveProperty('authToken')
  client = new ApiClient({ authToken: register.user.authToken })
})

test('get me', async () => {
  const user = await client.get('/user/me')

  expect(user.data).toHaveProperty('_key')
  expect(user.data).toHaveProperty('_key')
})

test('get user', async () => {
  const me = await client.get('/user/me')
  const user = await client.get(`/user/${me.data._key}`)

  expect(user.data).toHaveProperty('_key')
  expect(user.data).toHaveProperty('defaultAddress')
  expect(user.data.defaultAddress).toHaveProperty('street')
})

test('delete user', async () => {
  const user = await client.get('/user/me')
  const deletedUser = await client.del(`/user/${user.data._key}`)
  deletedUserKey = user.data._key

  expect(deletedUser.data._removed).toBe(true)
})

test('throw error for deleted user', async () => {
  try {
    await client.get(`/user/${deletedUserKey}`)
  } catch (e) {
    expect(e).toHaveProperty('eventId')
  }
})
