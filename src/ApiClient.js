import superagent from 'superagent'

const methods = ['get', 'post', 'put', 'patch', 'del']
const { TEST_URL } = process.env

function formatUrl(path) {
  const adjustedPath = path[0] !== '/' ? `/${path}` : path
  const apiHost = TEST_URL || 'http://localhost:4000/v1'
  return apiHost + adjustedPath
}

export default class ApiClient {
  constructor (args = {}) {
    const { authToken } = args
    this.authToken = authToken

    methods.forEach((method) =>
      this[method] = (path, { query, data, headers } = {}) => new Promise((resolve, reject) => {
        const url = formatUrl(path)
        const request = superagent[method](url)

        if (query) {
          request.query(query);
        }

        if (this.authToken) {
          request.set({ Authorization: this.authToken })
        }

        if (headers) {
          request.set(headers)
        }

        if (data) {
          request.send(data);
        }

        request.end((err, { body } = {}) => err ? reject(body || err) : resolve(body));
      }
    ));
  }

  hello () {}
}
