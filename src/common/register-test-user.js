import { randomBytes } from 'crypto'
import names from 'starwars-names'
import ApiClient from '../ApiClient'

const client = new ApiClient()

export const registerTestUser = async () => {
  const username = randomBytes(4).toString('hex')
  const fullName = names.random()

  const user = await client.post('/user', {
    data: {
      name: fullName,
      email: `${username}@me.com`,
      phone: "(604) 345-1048",
      password: '1q2w3e4r',
      dateOfBirth: '1987-09-11',
      profile: {
        diet: 'VEGAN',
        allergies: ['fibre', 'dihydrogenmonoxide']
      },
      address: {
        street: '1917 Ferndale st',
        unit: '103',
        city: 'Vancouver',
        postal: 'V5L1X9'
      }
    }
  })

  return {
    user: user.data,
    client: new ApiClient({ authToken: user.data.authToken })
  }
}

export default registerTestUser
