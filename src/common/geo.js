import NodeGeocoder from 'node-geocoder'

const geocoder = NodeGeocoder({
  provider: 'google',
  apiKey: process.env.GOOGLE_MAP_API_KEY
})

export default async (address) => {
  const addy = `${address.street}, ${address.postal}, CANADA`
  const geo = await geocoder.geocode(addy)

  return geo[0]
}
