import moment from 'moment'
import { burp } from 'burrp'

export const getMeal = () => {
  const startOfThisWeek = moment().startOf('week')
  const meals = ["breakfast", "lunch", "dinner"]

  const meal = meals[Math.floor(Math.random() * 10) % 3]
  const diet = Math.floor(Math.random() * 10) % 2 ? 'meat' : 'vegan'

  const date = startOfThisWeek.add(Math.floor(Math.random() * 10) % 7, 'days').format('YYYY-MM-DD')
  const expiration = startOfThisWeek.add(6, 'days').format('YYYY-MM-DD')

  return Promise.resolve({
    name: burp()[0],
    description: "You'll be in hell. Pie hell.",
    storage: "Nothing can contain it.",
    preperation: ["Remove from packaging", "Microwave 2 minutes."],
    expiration,
    // make a random day of the week the serving date
    date,
    calories: "666",
    nutrients: [
      {
        name: "fat",
        unit: "grams",
        color: "black",
        quantity: 0
      }
    ],
    meal,
    diet,
    ingredients: [
      "Hahahahaha"
    ],
    allergens: ['eggs', 'nuts']
  })
}
