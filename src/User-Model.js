import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import { Model, Edge } from './db'
import Address from './Address-Model'

export class ReceivesAt extends Edge {
  static schema = {
    instructions: { $type: String, optional: true }
  }
}

export class Orders extends Edge {
  static schema = {
    address: Address,
    instructions: { $type: String, optional: true }
  }
}

export default class User extends Model {
  static schema = {
    name: String,
    email: String,
    phone: String,
    password: String,
    active: Boolean,
    dateOfBirth: { $type: String, optional: true },
    role: { $type: String, enum: ['ADMIN', 'USER'] },
    defaultAddress: Address,
    profile: {
      vegan: { $type: Boolean, optional: true },
      diet: { $type: String, enum: ['VEGAN', 'REGULAR'], optional: true },
      size: { $type: String, enum: ['SMALL', 'MEDIUM', 'LARGE'], optional: true },
      restrictions: { $type: String, optional: true },
      allergies: { $type: [String], optional: true }
    },
    avatar: { $type: String, optional: true },
    resetToken: { $type: String, optional: true }
  }

  async token () {
    return jwt.sign({ email: this.email }, process.env.JWT_SECRET);
  }

  async savePassword () {
    return new Promise((resolve, reject) => {
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (er, hash) => {
          if (err || er) {
            return reject(err || er);
          }

          this.password = hash
          this.update()
          return resolve();
        });
      })
    })
  }

  async verifyPassword (password) {
    return new Promise((resolve) => {
      bcrypt.compare(password, this.password, (error, isMatch) => resolve(isMatch))
    })
  }
}
