import orm from 'ormjs'

const { ARANGODB_URL, DATABASE_NAME } = process.env

const { Model, Edge } = orm.connect({
  database: DATABASE_NAME,
  url: ARANGODB_URL
})

export { Model, Edge }
