import { Model } from './db'

export default class Address extends Model {
  static schema = {
    street: { $type: String },
    unit: { $type: String, optional: true },
    city: { $type: String },
    postal: { $type: String },
    instructions: { $type: String, optional: true },
    geo: {
      longitude: Number,
      latitude: Number
    },
    notes: { $type: String, optional: true }
  }
}
