import { Model } from './db'

class Image {
  constructor ({ url, created }) {
    this.url = url
    this.created = created
  }

  toJSON () {
    const { url, created } = this

    return { url, created }
  }

  static fromJSON (json) {
    return new Image(json)
  }
}

class Nutrient {
  constructor (data) {
    this.name = data.name
    this.quantity = data.quantity
    this.unit = data.unit
  }

  toJSON () {
    const { name, quantity, unit } = this

    return {
      name, unit, quantity
    }
  }

  static fromJSON (json) {
    return new Nutrient(json)
  }
}

export default class Meal extends Model {
  static schema = {
    images: { $type: [Image], optional: true },
    name: String,
    description: { $type: String, optional: true },
    storage: { $type: String, optional: true },
    preperation: { $type: [String], optional: true },
    expiration: { $type: String, optional: true },
    date: String,
    calories: { $type: String, optional: true },
    nutrients: { $type: [Nutrient], optional: true },
    diet: { $type: String, enum: ['vegan', 'meat'], optional: false },
    ingredients: { $type: [String], optional: true },
    allergens: { $type: [String], optional: true },
    meal: {
      $type: String,
      enum: ['breakfast', 'lunch', 'dinner'],
      optional: false
    },
    tags: { $type: [String], optional: true }
  }

  async addNutrient (nutrient) {
    const nutrients = await this.nutrients
    if (!this.nutrients) {
      this.nutrients = []
    }
    nutrients.push(nutrient)
    await this.save()
  }

  async removeNutrient (nutrient) {
    const nutrients = await this.nutrients
    const index = await nutrients.indexOf(nutrient)
    nutrients.splice(index, 1)
    await this.save()
  }
}
