import jwt from 'jsonwebtoken'
import Raven from 'raven'
import getGeo from './common/geo'

import User, { ReceivesAt } from './User-Model'
import Address from './Address-Model'

const { NODE_ENV } = process.env

const setUserContext = async (user) => (
  Raven.mergeContext({
    user: {
      name: user.name,
      email: user.email,
      phone: user.phone,
      _key: user._key,
      role: user.role
    }
  })
)

export async function authorize (...args) {
  const ctx = (args.length === 2) ? args[0] : args[1]
  const next = (args.length === 2) ? args[1] : args[2]
  const param = (args.length === 2) ? null : args[0]
  const { request: { headers } } = ctx

  if (!headers.authorization) {
    ctx.throw(401, 'NOT_AUTHORIZED');
  }

  const decrypted = await jwt.verify(headers.authorization, process.env.JWT_SECRET);
  const user = await User.findOne({ where: { email: decrypted.email } });

  if (decrypted && user) {
    setUserContext(user)
    ctx.user = user
  }

  if (param) {
    if (ctx.user._key !== param && ctx.user.role !== 'ADMIN') {
      ctx.throw(401, 'NOT_AUTHORIZED');
    }
  }

  await next()
}

export async function authorizeAdmin (ctx, next) {
  const { request: { headers }, params: { userId } } = ctx

  if (ctx.params.userId === 'me') {
    return
  }

  if (!headers.authorization) {
    ctx.throw(401, 'NOT_AUTHORIZED');
  }

  const decrypted = await jwt.verify(headers.authorization, process.env.JWT_SECRET);
  const user = await User.findOne({ where: { email: decrypted.email } });

  if (decrypted && user) {
    setUserContext(user)
    ctx.user = user
  }

  if (userId && userId === user._key) {
    await next()
  } else if (user.role !== 'ADMIN' && NODE_ENV !== 'development') {
    ctx.throw(401, 'NOT_AUTHORIZED');
  } else if (
    user.role === 'ADMIN' || NODE_ENV === 'development'
  ) {
    await next()
  }
}

export async function login (ctx, next) {
  const { email, password } = ctx.request.body;

  const user = await User.findOne({
    where: { email: email.toLowerCase() }
  })

  if (!user) {
    ctx.throw({ fields: { email: 'NOT_FOUND' } })
  }

  const match = await user.verifyPassword(password)
  await user.update()

  if (user && match) {
    ctx.status = 200;
    ctx.body = {
      data: {
        authToken: await user.token(),
        ...user,
        password: undefined,
        resetToken: undefined
      }
    }
    await next()
  } else {
    ctx.throw({ fields: { password: 'Incorrect' } })
  }
}

export async function getMe (ctx, next) {
  const { user } = ctx

  if (user) {
    ctx.status = 200;
    ctx.body = {
      data: {
        ...user,
        defaultAddress: await user.defaultAddress,
        password: undefined,
        resetToken: undefined
      }
    }
  } else if (!user) {
    ctx.status = 401;
    ctx.body = { code: 'USER_DOES_NOT_EXIST' };
  }

  await next()
}

export async function getUsers (ctx, next) {
  const { page, count } = ctx.request.query

  const limit = count || 10
  const skip = limit * (page ? page - 1 : 0)

  const users = await User.find({
    skip,
    limit,
    sort: 'user.createdAt DESC',
    $attributes: ['_key', '_id', 'name', 'createdAt', 'profile'],
    include: {
      model: Address,
      as: 'defaultAddress'
    }
  })

  if (users) {
    ctx.status = 200;
    ctx.body = {
      data: users
    };
  } else if (!this.user) {
    ctx.status = 401;
    ctx.body = { code: 'NO_USERS' };
  }

  await next()
}

export async function getUser (ctx, next) {
  const { userId } = ctx.params

  if (userId !== 'me') {
    const user = await User.findOne({
      where: { _key: userId },
      include: {
        model: Address,
        as: 'defaultAddress'
      }
    })

    if (user) {
      ctx.status = 200;
      ctx.body = {
        data: {
          ...user,
          password: undefined,
          resetToken: undefined
        }
      };
    }
  }

  await next()
}

export async function postUser (ctx, next) {
  const { user } = ctx

  const loc = await getGeo(user.address)

  const address = await Address.add({
    ...user.address,
    geo: loc
  })
  const newUser = await User.add({
    ...user,
    defaultAddress: address,
    phone: user.phone,
    active: true,
    role: 'USER'
  })
  await newUser.savePassword()
  await newUser.save()
  await ReceivesAt.add(newUser, address, {
    instructions: user.address.instructions
  })

  ctx.body = {
    data: {
      ...newUser,
      authToken: await newUser.token(),
      password: undefined
    }
  }
  ctx.status = 200

  await next()
}

export async function patchUser (ctx, next) {
  const { user } = ctx
  const { request: { body } } = ctx

  for (const key in body) {
    user[key] = body[key]
  }

  if (body.password) {
    await user.savePassword()
  }

  await user.save()

  ctx.body = {
    ...user.update(),
    password: undefined,
    authToken: await user.token()
  }
  ctx.status = 200

  await next()
}


export async function deleteUser (ctx, next) {
  const { deleteuser } = ctx.params

  const userToRemove = await User.findOne({ where: { _key: deleteuser } })

  if (!userToRemove) {
    ctx.throw('USER NOT FOUND')
  }

  await userToRemove.remove()

  ctx.body = {
    data: userToRemove
  }
  ctx.status = 200
  await next()
}

