import moment from 'moment'

import registerTestUser from './common/register-test-user'
import { getMeal } from './common/dummy-data'

let user = {}
let client = {}

beforeAll(async () => {
  const register = await registerTestUser()
  user = await register.user
  client = await register.client
})

test('place orders', async () => {
  await client.post('/meal', {
    data: {
      ...await getMeal(),
      date: moment().format('YYYY-MM-DD')
    }
  })
  const meals = await client.get('/meal')
  const orders = await client.post(`/user/${user._key}/orders`, {
    data: {
      orders: [meals.data[0]._key]
    }
  })

  expect(orders.data[0]).toHaveProperty('_key')
})

/* This test might need some fixing as I had it fail many times in a row, then work again after no change :shrug: */
test('get user orders', async () => {
  const orders = await client.get(`/user/${user._key}/orders`, {
    query: {
      start: moment().startOf('week').format(),
      end: moment().endOf('week').format()
    }
  })

  expect(orders.data[0]).toHaveProperty('_id')
  expect(orders.data[0]).toHaveProperty('address')
  expect(orders.data[0]).toHaveProperty('meal')
})

test('get orders and counts', async () => {
  const orders = await client.get('/orders', {
    query: {
      start: moment().startOf('week').format(),
      end: moment().endOf('week').format()
    }
  })

  expect(orders.meta.counts[0]).toHaveProperty('count')
  expect(orders.data[0]).toHaveProperty('user.allergies')
  expect(orders.data[0]).toHaveProperty('user.address')
  expect(orders.data[0]).toHaveProperty('user.email')
  expect(orders.data[0]).toHaveProperty('user.phone')
  expect(orders.meta.counts[0]).toHaveProperty('allergies')
})

test('delete order', async () => {
  const meal = await client.post('/meal', {
    data: {
      ...await getMeal(),
      date: moment().format('YYYY-MM-DD')
    }
  })
  await client.post(`/user/${user._key}/orders`, {
    data: {
      orders: [meal.data._key]
    }
  })
  const orders = await client.get('/orders')
  const order = orders.data[0].meals[0]
  const deletedOrder = await client.del(`/orders/${order._key}`)

  expect(deletedOrder.data._removed).toBe(true)
})
