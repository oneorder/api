import getGeo from './common/geo'

import { ReceivesAt } from './User-Model'
import Address from './Address-Model'

export async function postAddress (ctx, next) {
  const { user } = ctx
  const { body } = ctx.request

  const loc = await getGeo(body)
  const address = await Address.add({
    ...body,
    geo: loc
  })
  await ReceivesAt.add(user, address, {
    instructions: body.instructions
  })

  ctx.body = {
    data: address
  }
  ctx.status = 200

  await next()
}

export async function delAddress (ctx, next) {
  const { user } = ctx
  const { _id } = ctx.request.body

  const edge = await ReceivesAt.findOne({
    where: {
      _from: user._id,
      _to: _id
    }
  })
  await edge.remove()

  ctx.body = { data: edge }
  ctx.status = 200

  await next()
}

export async function patchAddress (ctx, next) {
  const { body } = ctx.request
  const { address } = ctx.params

  const requestedAddress = await Address.findOne({
    where: { _key: address }
  })

  for (const key in body) {
    switch (key) {
      case '_key':
      case '_id':
      case 'createdAt':
      case 'updatedAt':
        break
      default:
        requestedAddress[key] = body[key]
    }
  }

  await requestedAddress.save()
  await requestedAddress.update()

  ctx.body = {
    data: requestedAddress
  }
  ctx.status = 200

  await next()
}

export async function getAddresses (ctx, next) {
  const { page, count } = ctx.request.query

  const limit = count || 10
  const skip = limit * (page ? page - 1 : 0)

  const meals = await Address.find({
    skip,
    limit
  })

  ctx.body = { data: meals }
  ctx.status = 200

  await next()
}

