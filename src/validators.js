import { isAlphanumeric, isLength, isEmail, trim } from 'validator'

export async function userValidator (ctx, next) {
  const { body } = ctx.request

  const user = {
    name: trim(body.name).toLowerCase(),
    email: trim(body.email).toLowerCase(),
    password: trim(body.password),
    ...body,
    joined: Date()
  }

  if (!user.name) {
    ctx.throw({ fields: { name: 'NO_USERNAME' } })
  }
  if (!isLength(user.name, 3, 32)) {
    ctx.throw({ fields: { name: 'USERNAME_NOT_RIGHT_LENGTH' } })
  }

  if (!user.email) {
    ctx.throw({ fields: { email: 'NO_EMAIL' } })
  }
  if (!isEmail(user.email)) {
    ctx.throw({ fields: { email: 'EMAIL_NOT_VALID' } })
  }
  if (!isLength(user.email, 5, 255)) {
    ctx.throw({ fields: { email: 'EMAIL_NOT_RIGHT_LENGTH' } })
  }

  if (!user.password) {
    ctx.throw({ fields: { password: 'NO_PASSWORD' } })
  }
  if (!isLength(user.password, 4, 255)) {
    ctx.throw({ fields: { password: 'PASSWORD_NOT_LONG_ENOUGH' } })
  }

  ctx.user = user

  await next()
}
