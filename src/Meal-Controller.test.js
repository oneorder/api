import { burp } from 'burrp'

import registerTestUser from './common/register-test-user'
import { getMeal } from './common/dummy-data'

// let user = {}
let client = {}

beforeAll(async () => {
  const register = await registerTestUser()
  // user = register.user
  client = register.client
})

test('post meal', async () => {
  const randomMeal = await getMeal()
  const meal = await client.post('/meal', {
    data: randomMeal
  })

  expect(meal.data).toHaveProperty('_key')
  expect(meal.data.name).toBe(randomMeal.name)
})

test('get meals', async () => {
  const meals = await client.get('/meal')
  const meal = meals.data[0]

  expect(meal).toHaveProperty('_key')
  expect(meal).toHaveProperty('name')
})

test('get single meal', async () => {
  const meals = await client.get('/meal')
  const meal = meals.data[0]
  const singleMeal = await client.get(`/meal/${meal._key}`)

  expect(singleMeal.data).toHaveProperty('_key')
})

test('edit meal', async () => {
  const newName = burp()[0]
  const calories = Math.floor(Math.random() * Math.random() * 1000)
  const meals = await client.get('/meal')
  const meal = meals.data[0]
  const nutrients = meal.nutrients
  nutrients.pop()
  const newMeal = await client.patch(`/meal/${meal._key}`, {
    data: {
      name: newName,
      nutrients,
      calories
    }
  })

  expect(newMeal.data.name).toBe(newName)
  expect(newMeal.data.calories).toBe(calories)
})

test('delete meal', async () => {
  const meals = await client.get('/meal')
  const meal = meals.data[0]
  const del = await client.del(`/meal/${meal._key}`)

  expect(del.data._removed).toBe(true)
})
