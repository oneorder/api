import moment from 'moment'

import { Model } from './db'
import Meal from './Meal-Model'
import User, { Orders } from './User-Model'

export async function postOrders (ctx, next) {
  const { request: { body: { orders } }, params } = ctx
  let user = ctx.user

  if (user.role === 'ADMIN' && params.user) {
    user = await User.findOne({
      where: { _key: params.user }
    })
  }

  const newOrdersArray = await Promise.all(orders.map(async (order) => {
    const meal = await Meal.findOne({ where: { _key: order } })
    const newOrder = await Orders.add(user, meal, {
      address: await user.defaultAddress
    })

    if (newOrder) {
      return Promise.resolve(newOrder)
    }
  }))

  ctx.body = {
    data: newOrdersArray
  }
  ctx.status = 200

  await next()
}

export async function getOrders (ctx, next) {
  const { start, end } = ctx.request.query
  const db = await Model._database

  const rangeStart = start ? moment(start).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')
  const rangeEnd = end ? moment(end).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')

  const cursor = await db.query({
    query: `
      for user in User
        let meals = (
            for m, o in outbound user._id Orders
                filter o._removed != true
                filter m.date >= @rangeStart && m.date <= @rangeEnd
                return {
                  _key: o._key,
                  meal_key: m._key,
                  diet: m.diet,
                  meal: m.meal,
                  name: m.name,
                  date: m.date
                }
        )
        filter length(meals) > 0
        return {
            user: {
                _key: user._key,
                _id: user._id,
                name: user.name,
                email: user.email,
                phone: user.phone,
                allergies: user.profile.allergies,
                address: (
                    for address in Address 
                        filter address._id == user.defaultAddress 
                        return address
                )[0]
            },
            meals: meals
        }`,
    bindVars: { rangeStart, rangeEnd }
  })

  const mealsAndCounts = await db.query({
    query: `
      for meal in Meal
        filter meal.date >= @start && meal.date <= @end
        let meals = (
            for u, o in inbound meal._id Orders
                filter o._removed != true
                filter u._removed != true
                return u
        )
        let allergies = (
          for u, o in inbound meal._id Orders
              filter o._removed != true
              filter u._removed != true
              filter length(u.profile.allergies) > 0
              return {
                  name: u.name,
                  allergies: u.profile.allergies
              }
        )
        filter length(meals) > 0
        return {
            meal: meal,
            count: length(meals),
            allergies
        }
    `,
    bindVars: { start: rangeStart, end: rangeEnd }
  })

  ctx.body = {
    data: await cursor.all(),
    meta: {
      counts: await mealsAndCounts.all()
    }
  }
  ctx.status = 200

  await next()
}

export async function getUserOrders (ctx, next) {
  const { user } = ctx
  const { start, end } = ctx.request.query
  const db = await Model._database

  const rangeStart = start ? moment(start).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')
  const rangeEnd = end ? moment(end).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')

  const cursor = await db.query({
    query: `
      for meal, order in outbound @user Orders
        filter order._removed != true
        filter meal.date >= @start && meal.date <= @end
        for a in Address
            filter order.address == a._id
                return {
                    _id: order._id,
                    createdAt: order.createdAt,
                    updatedAt: order.updatedAt,
                    address: a,
                    meal: meal,
                    order: order
                }
    `,
    bindVars: {
      user: user._id,
      start: rangeStart,
      end: rangeEnd
    }
  })

  ctx.body = {
    data: await cursor.all()
  }
  ctx.status = 200

  await next()
}

export async function deleteOrder (ctx, next) {
  const { order } = ctx.params

  const deletedOrder = await Orders.findOne({
    where: { _key: order }
  })

  await deletedOrder.remove()

  ctx.body = {
    data: deletedOrder
  }
  ctx.status = 200

  await next()
}
