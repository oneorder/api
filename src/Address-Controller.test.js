import { randomBytes } from 'crypto'
import rand from 'randomatic'
import range from 'random-number-in-range'
import NodeGeocoder from 'node-geocoder'
import ApiClient from './ApiClient'

const geocoder = NodeGeocoder({
  provider: 'google'
})

const username = randomBytes(4).toString('hex')
let client = new ApiClient()
let user = {}
let address = {}

test('registration', async () => {
  const register = await client.post('/user', {
    data: {
      name: username,
      email: `${username}@me.com`,
      phone: "(123) 456-7890",
      password: '1q2w3e4r',
      profile: {
        vegan: true,
        allergies: ['fibre', 'dihydrogenmonoxide']
      },
      address: {
        street: '1917 Ferndale st',
        unit: '103',
        city: 'Vancouver',
        postal: 'V5L1X9'
      }
    }
  })

  user = register.data

  expect(register.data).toHaveProperty('authToken')
  client = new ApiClient({ authToken: register.data.authToken })
})

test('post address', async () => {
  address = await client.post(`/user/${user._key}/address`, {
    data: {
      street: '1917 Ferndale st',
      unit: '103',
      city: 'Vancouver',
      postal: 'V5L1X9'
    }
  })

  expect(address.data).toHaveProperty('city')
  expect(address.data).toHaveProperty('unit')
})

test('patch address', async () => {
  const lat = range(49281566, 49281118) / 1000000
  const lon = range(122952805, 122956924) / -1000000
  const geo = await geocoder.reverse({
    lat,
    lon
  })
  const updatedAddress = await client.patch(`/user/${user._key}/address/${address.data._key}`, {
    data: {
      street: `${geo[0].streetNumber} ${geo[0].streetName}`,
      city: geo[0].city,
      postal: geo[0].zipcode,
      notes: 'Ring buzzer'
    }
  })

  expect(updatedAddress.data.city).toBe(geo[0].city)
  expect(updatedAddress.data.notes).toBe('Ring buzzer')
})
