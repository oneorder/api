import Router from 'koa-router'
import { postUser, authorize, getMe, login, patchUser, getUsers, authorizeAdmin, deleteUser, getUser } from './User-Controller';
import { postAddress, delAddress, getAddresses, patchAddress } from './Address-Controller';
import { postMeal, getMeals, deleteMeal, patchMeal, getMeal } from './Meal-Controller';
import { postOrders, getOrders, getUserOrders, deleteOrder } from './Orders-Controller';
import { userValidator } from './validators'

export default () => {
  const prefix = '/v1'
  const router = new Router({ prefix })

  /* User */
  router.post('/user/', userValidator, postUser)
  router.patch('/user/:user', patchUser)
  router.get('/user/me', authorize, getMe)
  router.get('/user/:userId', authorizeAdmin, getUser)
  router.del('/user/:deleteuser', authorizeAdmin, deleteUser)
  router.post('/session', login)
  router.get('/user', authorizeAdmin, getUsers)

  /* Address */
  router.post('/user/:user/address', postAddress)
  router.del('/user/:user/address', delAddress)
  router.patch('/user/:user/address/:address', patchAddress)
  router.get('/address', authorizeAdmin, getAddresses)

  /* Meal */
  router.post('/meal', authorizeAdmin, postMeal)
  router.del('/meal/:meal', authorizeAdmin, deleteMeal)
  router.patch('/meal/:meal', authorizeAdmin, patchMeal)
  router.get('/meal/:meal', getMeal)
  router.get('/meal', getMeals)

  /* Order */
  router.post('/user/:user/orders', postOrders)
  router.get('/user/:user/orders', getUserOrders)
  router.get('/orders', authorizeAdmin, getOrders)
  router.del('/orders/:order', authorizeAdmin, deleteOrder)

  /* Authentication */
  router.param('user', authorize)

  return router.routes()
}
